package main

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"net/http"
	"sync"

	"github.com/gorilla/mux"
	"gitlab.com/javierexposito/redis-go/pkg/redis"
)

type App struct {
	redis *redis.MiniRedis
}

func main() {
	r := redis.MiniRedis{
		KeyMap:          make(map[string]string),
		SortedSetKeyMap: make(map[string]map[string]float32),
		Mutex:           sync.Mutex{},
	}

	app := App{
		redis: &r,
	}

	router := mux.NewRouter()

	router.HandleFunc("/{key}", app.GetValueHandler).Methods("GET")
	router.HandleFunc("/{key}", app.SetValueHandler).Methods("PUT")
	router.HandleFunc("/{key}", app.DeleteKeyHandler).Methods("DELETE")
	log.Fatal(http.ListenAndServe(":8080", router))
}

func (app *App) GetValueHandler(w http.ResponseWriter, r *http.Request) {
	key := r.URL.RequestURI()[1:]

	keyType := app.redis.TYPE(key)

	switch keyType {
	case "zset":
		createResponse(w, http.StatusOK, app.redis.ZRANGE(key, -1, -1))
		return
	case "string":
		value, err := app.redis.GET(key)
		if err != nil {
			w.WriteHeader(http.StatusNoContent)
			return
		}
		createResponse(w, http.StatusOK, value)
		return
	default:
		createResponse(w, http.StatusNoContent, nil)
	}

}

func (app *App) SetValueHandler(w http.ResponseWriter, r *http.Request) {
	key := r.URL.RequestURI()[1:]

	defer r.Body.Close()

	body, err := ioutil.ReadAll(r.Body)

	if err != nil {
		payload := map[string]string{"error": err.Error()}
		createResponse(w, http.StatusInternalServerError, payload)
	}

	var simpleValue string

	err = json.Unmarshal(body, &simpleValue)

	if err != nil {
		member := redis.Member{}
		err = json.Unmarshal(body, &member)

		if err != nil {
			list := redis.MemberList{}

			err = json.Unmarshal(body, &list)

			if err != nil {
				payload := map[string]string{"error": "invalid body format"}
				createResponse(w, http.StatusInternalServerError, payload)
			}

			updatedValues := app.redis.ZADD(key, list...)
			createResponse(w, http.StatusOK, updatedValues)
			return
		}

		app.redis.ZADD(key, redis.Member{Id: member.Id, Score: member.Score})
		w.WriteHeader(http.StatusOK)
		return
	}

	app.redis.SET(key, simpleValue, -1)
	w.WriteHeader(http.StatusOK)
}

func (app *App) DeleteKeyHandler(w http.ResponseWriter, r *http.Request) {
	key := r.URL.RequestURI()[1:]

	keyType := app.redis.TYPE(key)

	if keyType != "none" {
		app.redis.DEL(key)
		w.WriteHeader(http.StatusOK)
		return
	}

	w.WriteHeader(http.StatusNoContent)
}

func createResponse(w http.ResponseWriter, code int, payload interface{}) {
	response, _ := json.Marshal(payload)
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(code)
	_, _ = w.Write(response)
}
