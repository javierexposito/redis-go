package redis_test

import (
	"sync"
	"testing"
	"time"

	"math/rand"
	"strconv"

	"github.com/stretchr/testify/require"
	"gitlab.com/javierexposito/redis-go/pkg/redis"
)

func TestRedis_Get_Set(t *testing.T) {
	assert := require.New(t)

	r := redis.MiniRedis{
		KeyMap: make(map[string]string),
		Mutex:  sync.Mutex{},
	}

	key := "test"
	value := "test-value"

	assert.True(r.SET(key, value, -1))

	readValue, err := r.GET(key)
	assert.NoError(err)
	assert.Equal(value, readValue)
}

func TestRedis_Get_Set_with_expiration(t *testing.T) {
	assert := require.New(t)

	r := redis.MiniRedis{
		KeyMap: make(map[string]string),
		Mutex:  sync.Mutex{},
	}

	key := "test"
	value := "test-value"

	assert.True(r.SET(key, value, 1))

	readValue, err := r.GET(key)
	assert.NoError(err)
	assert.Equal(value, readValue)

	<-time.After(time.Second * time.Duration(1))
	_, err = r.GET(key)
	assert.Error(err, "key does not exist")
}

func TestRedis_Get_when_key_does_not_exist(t *testing.T) {
	assert := require.New(t)

	r := redis.MiniRedis{
		KeyMap: make(map[string]string),
		Mutex:  sync.Mutex{},
	}

	key := "fake-key"
	_, err := r.GET(key)

	assert.Error(err)
}

func TestRedis_DEL_returns_false_when_key_does_not_exist(t *testing.T) {
	assert := require.New(t)

	r := redis.MiniRedis{
		KeyMap: make(map[string]string),
		Mutex:  sync.Mutex{},
	}

	key := "fake-key"

	assert.False(r.DEL(key))
}

func TestRedis_DEL(t *testing.T) {
	assert := require.New(t)

	r := redis.MiniRedis{
		KeyMap: make(map[string]string),
		Mutex:  sync.Mutex{},
	}

	keyA := "key-" + strconv.Itoa(rand.Int())
	keyB := "key-" + strconv.Itoa(rand.Int())

	assert.True(r.SET(keyA, "some-value", -1))
	assert.True(r.SET(keyB, "some-value", -1))

	assert.True(r.DEL(keyA))

	_, err := r.GET(keyA)

	assert.Error(err)
}

func TestRedis_INCR_Fails_when_value_not_integer(t *testing.T) {
	assert := require.New(t)

	r := redis.MiniRedis{
		KeyMap: make(map[string]string),
		Mutex:  sync.Mutex{},
	}

	keyA := "key-" + strconv.Itoa(rand.Int())
	valueA := "0.5"

	assert.True(r.SET(keyA, valueA, -1))

	_, err := r.INCR(keyA)
	assert.Error(err)
}

func TestRedis_INCR_sets_zero_value_if_key_does_not_exist(t *testing.T) {
	assert := require.New(t)

	r := redis.MiniRedis{
		KeyMap: make(map[string]string),
		Mutex:  sync.Mutex{},
	}

	keyA := "key-" + strconv.Itoa(rand.Int())

	int, err := r.INCR(keyA)

	assert.NoError(err)
	assert.Equal(1, int)
}

func TestMiniRedis_ZADD(t *testing.T) {
	assert := require.New(t)

	key := "key-" + strconv.Itoa(rand.Int())

	r := redis.MiniRedis{
		SortedSetKeyMap: make(map[string]map[string]float32),
		Mutex:           sync.Mutex{},
	}

	m1 := redis.Member{
		Id:    "member-1",
		Score: 100,
	}

	m2 := redis.Member{
		Id:    "member-2",
		Score: 500,
	}

	m3 := redis.Member{
		Id:    "member-3",
		Score: 1000,
	}

	assert.Equal(3, r.ZADD(key, m1, m2, m3))

	assert.Equal(0, r.ZADD(key, m1))
}

func TestMiniRedis_ZRANK(t *testing.T) {
	assert := require.New(t)

	r := redis.MiniRedis{
		SortedSetKeyMap: make(map[string]map[string]float32),
		Mutex:           sync.Mutex{},
	}

	key := "key-" + strconv.Itoa(rand.Int())

	m1 := redis.Member{
		Id:    "member-1",
		Score: 200,
	}

	m2 := redis.Member{
		Id:    "member-2",
		Score: 10,
	}

	m3 := redis.Member{
		Id:    "member-3",
		Score: 1000,
	}

	assert.Equal(3, r.ZADD(key, m1, m2, m3))

	assert.Equal(1, r.ZRANK(key, m1.Id))
	assert.Equal(2, r.ZRANK(key, m2.Id))
	assert.Equal(0, r.ZRANK(key, m3.Id))

	// overwrite m2 value to set as Max score
	m2 = redis.Member{
		Id:    "member-2",
		Score: 2000,
	}

	assert.Equal(0, r.ZADD(key, m2))

	assert.Equal(2, r.ZRANK(key, m1.Id))
	assert.Equal(0, r.ZRANK(key, m2.Id))
	assert.Equal(1, r.ZRANK(key, m3.Id))

	assert.Equal(-1, r.ZRANK(key, "non-existent-member"))
}

func TestMiniRedis_ZCARD(t *testing.T) {
	assert := require.New(t)

	r := redis.MiniRedis{
		SortedSetKeyMap: make(map[string]map[string]float32),
		Mutex:           sync.Mutex{},
	}

	m1 := redis.Member{
		Id:    "member-1",
		Score: 200,
	}

	m2 := redis.Member{
		Id:    "member-2",
		Score: 10,
	}

	m3 := redis.Member{
		Id:    "member-3",
		Score: 1000,
	}

	keyA := "key-" + strconv.Itoa(rand.Int())
	keyB := "key-" + strconv.Itoa(rand.Int())

	r.ZADD(keyA, m1, m2)
	r.ZADD(keyB, m3)

	assert.Equal(2, r.ZCARD(keyA))
	assert.Equal(1, r.ZCARD(keyB))
}

func TestMiniRedis_ZRANGE(t *testing.T) {
	assert := require.New(t)

	r := redis.MiniRedis{
		SortedSetKeyMap: make(map[string]map[string]float32),
		Mutex:           sync.Mutex{},
	}

	m1 := redis.Member{
		Id:    "member-1",
		Score: 200,
	}

	m2 := redis.Member{
		Id:    "member-2",
		Score: 10,
	}

	m3 := redis.Member{
		Id:    "member-3",
		Score: 1000,
	}

	m4 := redis.Member{
		Id:    "member-4",
		Score: 500,
	}

	key := "key-" + strconv.Itoa(rand.Int())

	assert.Equal(4, r.ZADD(key, m1, m2, m3, m4))

	for _, testData := range []struct {
		start, stop    float32
		expectedLength int
	}{
		{start: 500, stop: 1000, expectedLength: 2},
		{start: 400, stop: 1200, expectedLength: 2},
		{start: 1200, stop: 2000, expectedLength: 0},
		{start: 600, stop: 2000, expectedLength: 1},
		{start: -1, stop: 2000, expectedLength: 4},
		{start: 400, stop: -1, expectedLength: 2},
		{start: -1, stop: -1, expectedLength: 4},
	} {
		sortedSet := r.ZRANGE(key, testData.start, testData.stop)
		assert.Len(sortedSet, testData.expectedLength)
	}
}

func TestMiniRedis_DBSIZE(t *testing.T) {
	assert := require.New(t)

	r := redis.MiniRedis{
		KeyMap:          make(map[string]string),
		SortedSetKeyMap: make(map[string]map[string]float32),
		Mutex:           sync.Mutex{},
	}

	assert.True(r.SET("key-a", "some-value", -1))
	assert.True(r.SET("key-b", "some-value", -1))

	assert.Equal(2, r.DBSIZE())

	r.ZADD("sorted-set-a", redis.Member{"a", 200}, redis.Member{"b", 300})
	r.ZADD("sorted-set-b", redis.Member{"c", 2000}, redis.Member{"d", 3000})

	assert.Equal(4, r.DBSIZE())

	r.DEL("key-a")
	r.DEL("sorted-set-a")

	assert.Equal(2, r.DBSIZE())

	r.DEL("key-b")
	r.DEL("sorted-set-b")

	assert.Equal(0, r.DBSIZE())
}
