package redis

import (
	"fmt"
	"sort"
	"strconv"
	"sync"
	"time"
)

type Keys interface {
	SET(key string, value string, EX int) bool
	GET(key string) (string, error)
	DEL(key string) bool
	INCR(key string) (int, error)
}

type Member struct {
	Id    string
	Score float32
}

type SortedSet interface {
	ZADD(key string, members ...Member) int
	ZCARD(key string) int
	ZSCORE(key, memberId string) (float32, error)
	ZRANK(key, member string) int
	ZRANGE(key string, start, stop float32) []Member
}

type Database interface {
	Keys
	SortedSet
	DBSIZE() int
	TYPE(key string) string
}

type MiniRedis struct {
	KeyMap          map[string]string
	SortedSetKeyMap map[string]map[string]float32
	Mutex           sync.Mutex
}

func (r *MiniRedis) DBSIZE() int {
	r.Mutex.Lock()
	defer r.Mutex.Unlock()

	return len(r.KeyMap) + len(r.SortedSetKeyMap)
}

func (r *MiniRedis) TYPE(key string) string {
	if _, ok := r.KeyMap[key]; ok {
		return "string"
	}

	if _, ok := r.SortedSetKeyMap[key]; ok {
		return "zset"
	}

	return "none"
}

// originally redis EX value is optional
func (r *MiniRedis) SET(key string, value string, EX int) bool {
	r.Mutex.Lock()
	defer r.Mutex.Unlock()

	r.KeyMap[key] = value

	if EX != -1 {

		go func(EX int) {
			time.Sleep(time.Duration(EX))
			r.DEL(key)
		}(EX)
	}

	return true
}

// originally redis returns nil if the key does not exist
func (r *MiniRedis) GET(key string) (string, error) {
	r.Mutex.Lock()
	defer r.Mutex.Unlock()

	if _, ok := r.KeyMap[key]; !ok {
		return "", fmt.Errorf("key does not exist")
	}

	return r.KeyMap[key], nil
}

func (r *MiniRedis) DEL(key string) bool {
	r.Mutex.Lock()
	defer r.Mutex.Unlock()

	if _, ok := r.KeyMap[key]; !ok {
		if _, ok := r.SortedSetKeyMap[key]; !ok {
			return false
		}

		delete(r.SortedSetKeyMap, key)

		return true
	}

	delete(r.KeyMap, key)

	return true
}

func (r *MiniRedis) INCR(key string) (int, error) {
	r.Mutex.Lock()
	defer r.Mutex.Unlock()

	if _, ok := r.KeyMap[key]; !ok {
		r.KeyMap[key] = "0"
	}

	intValue, err := strconv.Atoi(r.KeyMap[key])

	if err != nil {
		return 0, err
	}

	intValue++

	r.KeyMap[key] = strconv.Itoa(intValue)

	return intValue, nil
}

func (r *MiniRedis) ZADD(key string, members ...Member) int {
	r.Mutex.Lock()
	defer r.Mutex.Unlock()

	updatedMembers := 0

	for _, member := range members {
		if _, ok := r.SortedSetKeyMap[key]; !ok {
			r.SortedSetKeyMap[key] = make(map[string]float32)
		}

		if _, ok := r.SortedSetKeyMap[key][member.Id]; !ok {
			updatedMembers++
		}

		r.SortedSetKeyMap[key][member.Id] = member.Score
	}

	return updatedMembers
}

func (r *MiniRedis) ZSCORE(key, memberId string) (float32, error) {
	r.Mutex.Lock()
	defer r.Mutex.Unlock()

	if _, ok := r.SortedSetKeyMap[key]; !ok {
		return 0, fmt.Errorf("key does not exist")
	}

	if _, ok := r.SortedSetKeyMap[key][memberId]; !ok {
		return 0, fmt.Errorf("memberId does not exist")
	}

	return r.SortedSetKeyMap[key][memberId], nil
}

func (r *MiniRedis) ZCARD(key string) int {
	r.Mutex.Lock()
	defer r.Mutex.Unlock()

	if _, ok := r.SortedSetKeyMap[key]; !ok {
		return 0
	}

	return len(r.SortedSetKeyMap[key])
}

type MemberList []Member

func (l MemberList) Swap(i, j int)      { l[i], l[j] = l[j], l[i] }
func (l MemberList) Len() int           { return len(l) }
func (l MemberList) Less(i, j int) bool { return l[i].Score < l[j].Score }
func (l MemberList) Get(key string) int {
	for k, l := range l {
		if l.Id == key {
			return k
		}
	}
	return 0
}

// originally zrank returns nil if the member does not exist
func (r *MiniRedis) ZRANK(key, member string) int {
	r.Mutex.Lock()
	defer r.Mutex.Unlock()

	if _, ok := r.SortedSetKeyMap[key]; !ok {
		return -1
	}

	if _, ok := r.SortedSetKeyMap[key][member]; !ok {
		return -1
	}

	list := list(r.SortedSetKeyMap[key])

	sort.Sort(sort.Reverse(list))

	return list.Get(member)
}

func list(sortedSet map[string]float32) MemberList {
	list := make(MemberList, len(sortedSet))
	i := 0
	for key, score := range sortedSet {
		list[i] = Member{key, score}
		i++
	}

	return list
}

func (r *MiniRedis) ZRANGE(key string, start, stop float32) []Member {
	r.Mutex.Lock()
	defer r.Mutex.Unlock()

	if _, ok := r.SortedSetKeyMap[key]; !ok {
		return []Member{}
	}

	list := list(r.SortedSetKeyMap[key])

	sort.Sort(list)
	numElements := len(list)

	if (start != -1 && stop != -1) && (list[0].Score > stop || list[numElements-1].Score < start) {
		return []Member{}
	}

	startKey := 0
	endKey := numElements

	for key, member := range list {
		score := member.Score
		if start != -1 && score <= start || (score > start && key-1 >= 0 && list[key-1].Score < start) {
			startKey = key
		}

		if stop != -1 && score >= stop || (score < stop && key+1 < numElements && list[key+1].Score > stop) {
			endKey = key + 1
		}
	}

	return list[startKey:endKey]
}
