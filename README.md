# REDIS-GO

Redis (https://redis.io/) is an open source (BSD licensed), in-memory data structure store, used as a database. This is a simple redis implementation in golang just for fun. This implementation only supports a sub-set of the redis universe:
- SET key value
- SET key value EX seconds
- GET key
- DEL key
- DBSIZE
- TYPE key
- INCR key
- ZADD key score member
- ZCARD key
- ZRANK key member
- ZRANGE key start stop
- ZSCORE key member

### Testsuite

```
$ make test
```
### Linters

Run the gofmt, goimports and golangci-lint linters

```
$ make lint
```

### Compiling the project: linux / osx
```
$ make build-linux
$ make build-osx
```

### Run the project

Run the go project with (requires go 1.10.1):
```
$ make up
```
... Or simply compile the project and execute the binary:
```
$ make build-linux
$ ./redis-go
```

### HTTP NETWORK

To play with, there is a REST API that supports some of the redis functionalities:
- curl -d '"cool-value"' -X PUT localhost:8080/mykey
- curl -d '{"Id": "user-1", "Score": 100}' -X PUT localhost:8080/mysortedset
- curl -d '\[{"Id": "user-1", "Score": 100},{"Id": "user-2", "Score": 200}]' -X PUT localhost:8080/mysortedset
- curl -X DELETE localhost:8080/mykey