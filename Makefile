### Development
install-tools:
	go get -u github.com/golangci/golangci-lint/cmd/golangci-lint
	go get -u golang.org/x/tools/cmd/goimports

lint:
	@echo "→ Running gofmt..."
	@gofmt -w -l -s .
	@echo "→ Running goimports..."
	@goimports -w -l .
	@echo "→ Running golangci-lint..."
	@golangci-lint run -D gas -E interfacer -E unconvert -E goconst

lint-ci:
	golangci-lint run -D gas -E goimports -E interfacer -E unconvert -E goconst --deadline=5m

.PHONY: install-tools lint lint-ci

### Tests
test:
	@echo "→ Running tests..."
	go test -v ./...

.PHONY: test

### Server
up:
	@echo "→ Running server..."
	go run -race main.go

.PHONY: up

build-linux:
	GOOS=linux GOARCH=amd64 go build
.PHONY: build-linux

build-osx:
	GOOS=darwin GOARCH=amd64 go build
.PHONY: build-osx