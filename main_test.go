package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"
	"net/http/httptest"
	"strconv"
	"sync"
	"testing"

	"math/rand"

	"io/ioutil"

	"github.com/stretchr/testify/require"
	"gitlab.com/javierexposito/redis-go/pkg/redis"
)

func TestApp_GetValueHandler_NoContentWhenKeyDoesNotExist(t *testing.T) {
	assert := require.New(t)

	r := redis.MiniRedis{
		KeyMap:          make(map[string]string),
		SortedSetKeyMap: make(map[string]map[string]float32),
		Mutex:           sync.Mutex{},
	}

	app := App{
		redis: &r,
	}

	key := "test-key-" + strconv.Itoa(rand.Int())

	req, err := http.NewRequest("GET", fmt.Sprintf("/%s", key), nil)
	assert.NoError(err)

	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(app.GetValueHandler)
	handler.ServeHTTP(rr, req)

	assert.Equal(http.StatusNoContent, rr.Code)
}

func TestApp_GetValueHandler(t *testing.T) {
	assert := require.New(t)

	r := redis.MiniRedis{
		KeyMap:          make(map[string]string),
		SortedSetKeyMap: make(map[string]map[string]float32),
		Mutex:           sync.Mutex{},
	}

	app := App{
		redis: &r,
	}

	key := "test-key-" + strconv.Itoa(rand.Int())
	value := "string-value"

	assert.True(r.SET(key, value, -1))

	req, err := http.NewRequest("GET", fmt.Sprintf("/%s", key), nil)
	assert.NoError(err)

	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(app.GetValueHandler)
	handler.ServeHTTP(rr, req)

	assert.Equal(http.StatusOK, rr.Code)

	body, err := ioutil.ReadAll(rr.Body)
	assert.NoError(err)
	var response string

	err = json.Unmarshal(body, &response)
	assert.NoError(err)

	assert.Equal(value, response)
}

func TestApp_SetValueHandler_SetSimpleKeyOk(t *testing.T) {
	assert := require.New(t)

	r := redis.MiniRedis{
		KeyMap:          make(map[string]string),
		SortedSetKeyMap: make(map[string]map[string]float32),
		Mutex:           sync.Mutex{},
	}

	app := App{
		redis: &r,
	}

	key := "test-key-" + strconv.Itoa(rand.Int())
	value := "some-value"

	body, err := json.Marshal(value)
	assert.NoError(err)

	req, err := http.NewRequest("PUT", fmt.Sprintf("/%s", key), bytes.NewBuffer(body))
	assert.NoError(err)

	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(app.SetValueHandler)
	handler.ServeHTTP(rr, req)

	assert.Equal(http.StatusOK, rr.Code)

	readValue, err := app.redis.GET(key)
	assert.NoError(err)
	assert.Equal(value, readValue)
}

func TestApp_SetValueHandler_SetSingleSortedSetMemberOk(t *testing.T) {
	assert := require.New(t)

	r := redis.MiniRedis{
		KeyMap:          make(map[string]string),
		SortedSetKeyMap: make(map[string]map[string]float32),
		Mutex:           sync.Mutex{},
	}

	app := App{
		redis: &r,
	}

	key := "test-key-" + strconv.Itoa(rand.Int())
	memberId := "user-1"
	score := float32(100)
	member := redis.Member{Id: memberId, Score: score}

	body, err := json.Marshal(member)
	assert.NoError(err)

	req, err := http.NewRequest("PUT", fmt.Sprintf("/%s", key), bytes.NewBuffer(body))
	assert.NoError(err)

	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(app.SetValueHandler)
	handler.ServeHTTP(rr, req)

	assert.Equal(http.StatusOK, rr.Code)

	readScore, err := app.redis.ZSCORE(key, memberId)
	assert.NoError(err)
	assert.Equal(score, readScore)
}

func TestApp_SetValueHandler_SetSortedSetMemberListOk(t *testing.T) {
	assert := require.New(t)

	r := redis.MiniRedis{
		KeyMap:          make(map[string]string),
		SortedSetKeyMap: make(map[string]map[string]float32),
		Mutex:           sync.Mutex{},
	}

	app := App{
		redis: &r,
	}

	key := "test-key-" + strconv.Itoa(rand.Int())

	memberId1 := "user-1"
	score1 := float32(100)

	memberId2 := "user-2"
	score2 := float32(50)

	memberId3 := "user-3"
	score3 := float32(200)

	list := redis.MemberList{
		{memberId1, score1},
		{memberId2, score2},
		{memberId3, score3},
	}

	body, err := json.Marshal(list)
	assert.NoError(err)

	req, err := http.NewRequest("PUT", fmt.Sprintf("/%s", key), bytes.NewBuffer(body))
	assert.NoError(err)

	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(app.SetValueHandler)
	handler.ServeHTTP(rr, req)

	assert.Equal(http.StatusOK, rr.Code)

	readScore1, err := app.redis.ZSCORE(key, memberId1)
	assert.NoError(err)
	assert.Equal(score1, readScore1)

	readScore2, err := app.redis.ZSCORE(key, memberId2)
	assert.NoError(err)
	assert.Equal(score2, readScore2)

	readScore3, err := app.redis.ZSCORE(key, memberId3)
	assert.NoError(err)
	assert.Equal(score3, readScore3)
}

func TestApp_DeleteKeyHandler_OK(t *testing.T) {
	assert := require.New(t)

	r := redis.MiniRedis{
		KeyMap:          make(map[string]string),
		SortedSetKeyMap: make(map[string]map[string]float32),
		Mutex:           sync.Mutex{},
	}

	app := App{
		redis: &r,
	}

	key := "test-key-" + strconv.Itoa(rand.Int())
	value := "value-test"

	body, err := json.Marshal(value)
	assert.NoError(err)

	req, err := http.NewRequest("PUT", fmt.Sprintf("/%s", key), bytes.NewBuffer(body))
	assert.NoError(err)

	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(app.SetValueHandler)
	handler.ServeHTTP(rr, req)

	assert.Equal(http.StatusOK, rr.Code)

	req, err = http.NewRequest("DELETE", fmt.Sprintf("/%s", key), nil)
	assert.NoError(err)

	rr = httptest.NewRecorder()
	handler = http.HandlerFunc(app.DeleteKeyHandler)
	handler.ServeHTTP(rr, req)

	assert.Equal(http.StatusOK, rr.Code)

	assert.Equal("none", app.redis.TYPE(key))

	// when key does not exist, status code 204
	rr = httptest.NewRecorder()
	handler.ServeHTTP(rr, req)
	assert.Equal(http.StatusNoContent, rr.Code)
}
